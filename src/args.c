#include "args.h"

void handleArgs(ambiente_t *a, int argc, string_t argv[]) {
  // Tratamento dos argumentos
  int i = 1, h = 0;

  while (i < argc) {
    // Pega o file name out
    if (!strcmp(argv[i], "-f")) {
      i++;
      a->fileNameIn = (char *) calloc((strlen(argv[i]) + 1), sizeof(char));
      strcpy(a->fileNameIn, argv[i]);
    }

    // Pega o default directory
    else if (!strcmp(argv[i], "-o")) {
      i++;
      a->currentDir = (char *) calloc((strlen(argv[i]) + 1), sizeof(char));
      strcpy(a->currentDir, argv[i]);
    }

    // Pega os parametros iniciais
    else {
      a->params[h] = (char *) calloc(strlen(argv[i]) + 1, sizeof(char));
      strcpy(a->params[h], argv[i]);
      h++;
    }

    i++;
  }
}