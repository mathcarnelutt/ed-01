#ifndef ARGS
#define ARGS

#include <stdlib.h>
#include <string.h>
#include "types/types.h"

void handleArgs(ambiente_t *a, int argc, string_t argv[]);

#endif