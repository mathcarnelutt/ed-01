#include "comandos.h"

const struct {
  char *identificador;
  comando_enum_t cst;
} indice_comandos[] = {{"+x", ADD_X},
                       {"+y", ADD_Y},
                       {"+z", ADD_Z},
                       {"x", CLEAR_X},
                       {"y", CLEAR_Y},
                       {"z", CLEAR_Z},
                       {"d", CHANGE_DIR},
                       {"a", CHANGE_FILE_NAME},
                       {"e", CHANGE_FILE_EXT},
                       {"c", CREATE_FILE},
                       {"l", READ_FILE},
                       {"o", APPEND_FILE},
                       {"f", CLOSE_FILE},
                       {"w", WRITE_LINE},
                       {"r", READ_LINE}};

comando_t *lerComando(string_t linha) {
  if (!strcmp(linha, ""))
    return NULL;

  comando_t *comm = (comando_t *) calloc(1, sizeof(comando_t));
  comm->tipo      = NONE;
  comm->params[0] = NULL;
  comm->params[1] = NULL;

  string_t linhaCopy = (string_t) calloc(strlen(linha) + 1, sizeof(char));
  strcpy(linhaCopy, linha);

  string_t item = strtok(linhaCopy, " ");

  int h = 0;
  while (item && h < 2) {
    // Se for o primeiro item (comando)
    if (comm->tipo == NONE) {
      int numero_comandos =
        sizeof(indice_comandos) / sizeof(indice_comandos[0]);

      for (int i = 0; i < numero_comandos; i++) {
        if (!strcmp(item, indice_comandos[i].identificador)) {
          comm->tipo = indice_comandos[i].cst;
          break;
        }
      }

    }

    // Argumentos
    else {
      // Se tiver aspas
      if (item[0] == '"') {
        string_t param = (string_t) calloc(strlen(linha), sizeof(char));

        strcat(param, item);
        item = strtok(NULL, "\"");
        if (item) {
          strcat(param, " ");
          strcat(param, item);
        }

        removeChar(param, '"');

        comm->params[h] = (string_t) calloc(strlen(param) + 1, sizeof(char));
        strcpy(comm->params[h], param);
        free(param);
      }

      // Se for texto
      else if (comm->tipo == WRITE_LINE && item[0] != '#' && h > 0) {
        string_t param = (string_t) calloc(strlen(linha) + 1, sizeof(char));

        strcat(param, item);
        strcat(param, " ");

        item = strtok(NULL, "$");

        strcat(param, item);

        comm->params[h] = (string_t) calloc(strlen(param) + 1, sizeof(char));
        strcpy(comm->params[h], param);
        free(param);

      }

      // -Todo o resto
      else {
        comm->params[h] = (string_t) calloc(strlen(item) + 1, sizeof(char));
        strcpy(comm->params[h], item);
      }
      h++;
    }

    item = strtok(NULL, " ");
  }

  free(linhaCopy);
  free(item);
  // free(linha);

  return comm;
}

void executarComando(ambiente_t *ambiente, comando_t *comando) {
  int idFile;
  string_t path = NULL;

  if (!comando)
    return;

  switch (comando->tipo) {
    case CLEAR_X: clearVar(&ambiente->x); break;
    case CLEAR_Y: clearVar(&ambiente->y); break;
    case CLEAR_Z: clearVar(&ambiente->z); break;

    case ADD_X: concatVar(&ambiente->x, comando->params[0]); break;
    case ADD_Y: concatVar(&ambiente->y, comando->params[0]); break;
    case ADD_Z:
      // Se for a string
      if (comando->params[0][0] != '*') {
        concatVar(&ambiente->z, comando->params[0]);
      } else {
        int repeatCount;

        string_t copia =
          (string_t) calloc(strlen(comando->params[0]) + 1, sizeof(char));
        strcpy(copia, comando->params[0]);

        string_t numString = strtok(copia, "*");

        repeatCount = stringToInt(numString);

        if (repeatCount < 0) {
          printf("Erro ao converter \"%s\" para numero.\n", numString);
          printf("Numeros negativos sao invalidos");
          exit(EXIT_FAILURE);
        }

        free(copia);

        numString = (string_t) calloc(strlen(ambiente->z) + 1, sizeof(char));
        strcpy(numString, ambiente->z);

        // Repetir o conteudo repeatCount vezes
        for (int i = 0; i < repeatCount; i++)
          concatVar(&ambiente->z, numString);

        free(numString);
      }
      break;

    case CHANGE_DIR:
      changeVar(&ambiente->currentDir, comando->params[0]);
      break;
    case CHANGE_FILE_NAME:
      changeVar(&ambiente->fileNameOut, comando->params[0]);
      break;
    case CHANGE_FILE_EXT:
      changeVar(&ambiente->fileExt, comando->params[0]);
      break;

    case CREATE_FILE:
      idFile = stringToInt(comando->params[0]) - 1;

      path = createPath(
        ambiente->currentDir, ambiente->fileNameOut, ambiente->fileExt);
      FILE *newArq = createFile(path, WRITE);
      if (!newArq)
        printf("Erro ao criar o arquivo (linha %d)\n", ambiente->linhaAtual);
      ambiente->arqOut[idFile] = newArq;

      break;

    case READ_FILE:
      idFile = stringToInt(comando->params[0]) - 1;

      path = createPath(
        ambiente->currentDir, ambiente->fileNameOut, ambiente->fileExt);
      ambiente->arqOut[idFile] = createFile(path, READ);
      if (!ambiente->arqOut[idFile])
        printf("Erro ao abrir o arquivo (linha %d)\n", ambiente->linhaAtual);

      break;

    case APPEND_FILE:
      idFile = stringToInt(comando->params[0]) - 1;

      path = createPath(
        ambiente->currentDir, ambiente->fileNameOut, ambiente->fileExt);
      ambiente->arqOut[idFile] = createFile(path, APPEND);
      if (!ambiente->arqOut[idFile])
        printf("Erro ao abrir o arquivo (linha %d)\n", ambiente->linhaAtual);

      break;

    case CLOSE_FILE:
      // Fecha o arquivo de ID idFile
      idFile = stringToInt(comando->params[0]) - 1;
      if (ambiente->arqOut[idFile])
        fclose(ambiente->arqOut[idFile]);
      else
        printf(
          "Erro ao fechar arquivo: Nao ha um arquivo aberto no id %d (linha "
          "%d)\n",
          idFile,
          ambiente->linhaAtual);
      ambiente->arqOut[idFile] = NULL;
      break;

    case WRITE_LINE:
      idFile = stringToInt(comando->params[0]) - 1;
      if (!ambiente->arqOut[idFile])
        printf(
          "Erro ao escrever linha: Nao ha um arquivo aberto no id %d (linha "
          "%d)\n",
          idFile,
          ambiente->linhaAtual);
      else
        fprintf(ambiente->arqOut[idFile], "%s\n", comando->params[1]);
      break;

    case READ_LINE:
      idFile         = stringToInt(comando->params[0]) - 1;
      string_t linha = NULL;

      // Se nao há um arquivo aberto
      if (!ambiente->arqOut[idFile]) {
        printf(
          "Erro ao ler linha: Nao ha um arquivo aberto no id %d (linha %d)\n",
          idFile,
          ambiente->linhaAtual);
        break;
      }

      lerLinha(&linha, ambiente->arqOut[idFile]);

      switch (comando->params[1][1]) {
        case 'x': changeVar(&ambiente->x, linha); break;
        case 'y': changeVar(&ambiente->y, linha); break;
        case 'z': changeVar(&ambiente->z, linha); break;
      }

      if (linha)
        free(linha);

      break;

    // Default command
    default: break;
  }

  if (path)
    free(path);
}

void evaluateVars(ambiente_t *ambiente, comando_t *comando) {
  // Se nao for nenhum comando
  if (!comando)
    return;

  // Nesse comando deve armazenar o valor numa variável
  if (comando->tipo == READ_LINE)
    return;

  int i = 0, indice;

  while (comando->params[i] != NULL && i < 2) {
    // Se for um dos parametros
    if (comando->params[i][0] == '@') {
      indice = (int) comando->params[i][1] - '1';
      changeVar(&comando->params[i], ambiente->params[indice]);
    } else

      // Se for uma das variaveis
      if (comando->params[i][0] == '#') {
      switch (comando->params[i][1]) {
        case 'x': changeVar(&comando->params[i], ambiente->x); break;
        case 'y': changeVar(&comando->params[i], ambiente->y); break;
        case 'z': changeVar(&comando->params[i], ambiente->z); break;
      }
    } else

      // Se for o multiplicador do +z
      if (!strcmp(comando->params[i], "**")) {
      string_t newParam =
        (string_t) calloc(strlen(ambiente->params[0]) + 2, sizeof(char));
      strcat(newParam, "*");
      strcat(newParam, ambiente->params[0]);

      changeVar(&comando->params[i], newParam);

      free(newParam);
    } else {
      string_t newParam;

      while ((newParam = replace_str(comando->params[i], "\\n", "\n"))) {
        changeVar(&comando->params[i], newParam);
        if (newParam)
          free(newParam);
      }
    }

    i++;
  }
}

void clearVar(string_t *var) {
  free(*var);
  *var = (string_t) calloc(1, sizeof(char));
}

/**
 * Copia para a variavel var o conteudo da variavel newVar
 */
void changeVar(string_t *var, string_t newVar) {
  if (*var) {
    free(*var);
    *var = NULL;
  }

  *var = (string_t) calloc(strlen(newVar) + 1, sizeof(char));
  strcpy(*var, newVar);
}

void concatVar(string_t *var, string_t newVar) {
  size_t newSize = strlen(*var) + strlen(newVar) + 1;

  string_t newString = (string_t) calloc(newSize, sizeof(char));

  strcpy(newString, *var);
  strcat(newString, newVar);

  free(*var);

  *var = newString;
}