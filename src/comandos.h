#ifndef COMANDOS
#define COMANDOS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "files/files.h"
#include "types/types.h"
#include "utils.h"

comando_t *lerComando(string_t linha);

void executarComando(ambiente_t *ambiente, comando_t *comando);

void evaluateVars(ambiente_t *ambiente, comando_t *comando);

void clearVar(string_t *var);

void changeVar(string_t *var, string_t newVar);

void concatVar(string_t *var, string_t newVar);

#endif /* COMANDOS */