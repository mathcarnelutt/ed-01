#include "files.h"

int lerLinha(string_t *entrada, FILE *arq) {
  // Pega o tamanho da linha para o calloc
  int length_linha = tamanho_linha(arq);

  if (length_linha < 0)
    return 0;

  // Aloca a memoria de acordo com o tamanho
  if (*entrada)
    free(*entrada);
  *entrada = (string_t) calloc(length_linha, sizeof(char));

  // Lee a linha do arquivo
  fgets(*entrada, length_linha + 1, arq);

  // Move o ponteiro do arquivo à frente para nao entrar em loop
  fseek(arq, 1, SEEK_CUR);

  return 1;
}

FILE *createFile(string_t path, file_enum_t type) {
  string_t args = (string_t) calloc(2, sizeof(char));
  switch (type) {
    case READ: strcpy(args, "r"); break;
    case WRITE: strcpy(args, "w"); break;
    case APPEND: strcpy(args, "a"); break;
  }

  // if (type == WRITE || type == APPEND) {
  //   createFullDir(getDir(path));
  // }

  FILE *a = fopen(path, args);

  // Deu erro ao criar o arquivo
  if (!a) {
    printf("Erro ao criar/abrir o arquivo \"%s\".\n", path);
    exit(EXIT_FAILURE);
  }

  free(args);

  return a;
}

/**
 * Recebe o diretorio, o filename e o fileext e retorna o path completo para o
 * arquivo.
 *
 * Exemplo:
 * createPath("./home", "exemplo", "txt");
 * retorna "./home/exemplo.txt"
 *
 */
string_t createPath(string_t dir, string_t fileName, string_t fileExt) {
  string_t fullPath;

  fullPath = (string_t) calloc(
    strlen(dir) + strlen(fileName) + strlen(fileExt) + 3, sizeof(char));

  strcpy(fullPath, "");
  strcat(fullPath, dir);
  if (dir[strlen(dir) - 1] != '/')
    strcat(fullPath, "/");
  strcat(fullPath, fileName);
  strcat(fullPath, ".");
  strcat(fullPath, fileExt);

  return fullPath;
}

void createFullDir(string_t dir) {
  string_t fullPath = (string_t) calloc(strlen(dir) + 2, sizeof(char));

  if (dir[0] == '/')
    strcat(fullPath, "/");

  string_t item = strtok(dir, "/");

  while (item) {
    strcat(fullPath, item);
    strcat(fullPath, "/");

    mkdir(fullPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    item = strtok(NULL, "/");
  }

  free(fullPath);
}

string_t getDir(string_t path) {
  string_t lineCopy = (string_t) calloc(strlen(path) + 1, sizeof(char));
  string_t dir      = (string_t) calloc(strlen(path) + 1, sizeof(char));
  strcpy(lineCopy, path);

  int barraCount = countChar(path, '/');

  int h = 0;

  string_t item = strtok(lineCopy, "/");
  while (item) {
    strcat(dir, item);

    if (++h >= barraCount)
      break;

    strcat(dir, "/");

    item = strtok(NULL, "/");
  }

  dir = (string_t) realloc(dir, (strlen(dir) + 1) * sizeof(char));

  free(lineCopy);

  return dir;
}

string_t getFilename(string_t path);
string_t getFileExt(string_t path);

int tamanho_linha(FILE *arq) {
  int tam = 0;
  char c;

  while ((c = fgetc(arq)) != '\n' && !feof(arq)) {
    tam++;
  }

  if (!tam)
    return -1;

  fseek(arq, -(tam + 1), SEEK_CUR);

  return tam;
}