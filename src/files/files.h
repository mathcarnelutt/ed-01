#ifndef FILES
#define FILES

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "../types/types.h"
#include "../utils.h"

typedef enum { READ, WRITE, APPEND } file_enum_t;

// TODO: Implementar essas tres funções
string_t getDir(string_t path);
string_t getFilename(string_t path);
string_t getFileExt(string_t path);

int lerLinha(string_t *entrada, FILE *arq);

FILE *createFile(string_t path, file_enum_t type);

void createFullDir(string_t dir);

string_t createPath(string_t dir, string_t fileName, string_t fileExt);

int tamanho_linha(FILE *arq);

#endif