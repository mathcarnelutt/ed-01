#include "ambiente.h"

ambiente_t *newAmbiente() {
  ambiente_t *a = (ambiente_t *) calloc(1, sizeof(ambiente_t));

  a->x = (string_t) calloc(1, sizeof(char));
  a->y = (string_t) calloc(1, sizeof(char));
  a->z = (string_t) calloc(1, sizeof(char));

  a->params[0]   = NULL;
  a->params[1]   = NULL;
  a->params[2]   = NULL;
  a->currentDir  = NULL;
  a->fileNameIn  = NULL;
  a->fileNameOut = NULL;
  a->fileExt     = NULL;

  a->arqIn = NULL;
  for (int i = 0; i < 10; i++)
    a->arqOut[i] = NULL;

  a->linhaAtual = 0;

  return a;
}

void freeAmbiente(ambiente_t **a) {
  free((*a)->x);
  free((*a)->y);
  free((*a)->z);
  free((*a)->params[0]);
  free((*a)->params[1]);
  free((*a)->params[2]);
  free((*a)->currentDir);
  free((*a)->fileNameIn);
  free((*a)->fileNameOut);
  free((*a)->fileExt);

  for (int i = 0; i < 10; i++)
    if ((*a)->arqOut[i])
      free((*a)->arqOut[i]);

  free(*a);

  *a = NULL;
}
