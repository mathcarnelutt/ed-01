#ifndef COMANDO_TYPE
#define COMANDO_TYPE

#include "types.h"

typedef enum {
  NONE,
  ADD_X,
  ADD_Y,
  ADD_Z,
  CLEAR_X,
  CLEAR_Y,
  CLEAR_Z,
  CHANGE_DIR,
  CHANGE_FILE_NAME,
  CHANGE_FILE_EXT,
  CREATE_FILE,
  APPEND_FILE,
  READ_FILE,
  CLOSE_FILE,
  WRITE_LINE,
  READ_LINE
} comando_enum_t;

typedef struct {
  comando_enum_t tipo;
  string_t params[2];
} comando_t;

#endif