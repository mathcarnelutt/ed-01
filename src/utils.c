#include "utils.h"

int stringToInt(string_t text) {
  int result = atoi(text);

  if (result < 0)
    return -2;

  if (result == 0) {
    if (!(text[0] >= '0' && text[0] <= '9'))
      return -1;
    else
      return 0;
  }

  return result;
}

void shiftLeft(string_t text, int n) {
  for (int i = 0; i < strlen(text) - n + 1; i++)
    text[i] = text[i + n];
}

/**
 * Remove todas as aparições de um char c na string text
 */
void removeChar(string_t text, char c) {
  int i   = 0, h;
  int tam = strlen(text);
  for (i = 0; i < tam; i++) {
    if (text[i] == c) {
      for (h = i; h < tam; h++)
        text[h] = text[h + 1];
      i--;
    }
  }
}

int countChar(string_t text, char c) {
  int cont, i, tam;
  tam  = strlen(text);
  cont = 0;
  for (i = 0; i < tam; i++)
    cont += (text[i] == c);

  return cont;
}

string_t replace_str(string_t str, string_t orig, string_t rep) {
  size_t size     = strlen(str) + strlen(rep) + 1;
  string_t buffer = (string_t) calloc(size, sizeof(char));

  string_t subs = strstr(str, orig);

  if (!subs) {
    free(buffer);
    return NULL;
  }

  strncpy(buffer, str, subs - str);
  buffer[subs - str] = '\0';

  sprintf(buffer + (subs - str), "%s%s", rep, subs + strlen(orig));

  return buffer;
}