#ifndef UTILS
#define UTILS

#include <stdlib.h>
#include <string.h>
#include "types/types.h"

int stringToInt(string_t text);

void shiftLeft(string_t text, int n);

void removeChar(string_t text, char c);

int countChar(string_t text, char c);

string_t replace_str(string_t str, string_t orig, string_t rep);

#endif /* UTILS */